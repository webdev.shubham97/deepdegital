<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta name="Description" content="Affordable Responsive Website Designing. Dedicated, VPS, Shared Web Hostings, Internet Marketing, Software Development, IT Web Security & Much More Services" />
    <meta name="Keywords" content="azeosoft, azeosoft web technologies, web designing, web design, website design, website designing, web development, website development, application development, software development, web designing company in lucknow, web development company in lucknow, web development allahabad, website development company allahabad, website development company allahabad, internet marketing, online marketing, online advertising company, Web security, IT web security, website scanner, software testing,e CRM services, ERP software, CRM software, HR management system, web hosting, shared web hosting , web hosting in lucknow, web hosting in allahabad, VPS hosting, virtual private server hosting, Dedicated server hosting, game server hosting, counter strike servers, minecraft servers, shoutcst hosting, online radio hosting, RDP hosting, remote desktop connection hosting, shared RDP, linux rdp, windows RDP, azeosoft.com" /> -->
<!-- ------------->

<?php

require_once('./includes/__titles.php');

$meta_data = meta_data();
//print_r($meta_data);

?>
<title><?= $meta_data[0] ?></title>
	<meta name="description" content="<?= $meta_data[1] ?>">
	<meta name="keywords" content="<?= $meta_data[2] ?>">
	<link rel="canonical" href="<?= $meta_data[3] ?>"/>
	<meta name="robots" content="index, follow">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="English">

    <!-- Required meta tags -->
	<meta name="theme-color" content="#e89897">
<!-- ----------- -->
    <title>IT Soft Agency</title>

    <!-- Bootstrap Links -->
    <link rel="stylesheet" href="./assets/css/bootstrap.css">
    <!-- Font Awesome Link -->
    <link rel="stylesheet" href="./assets/css/font-awesome.css">
    <!-- Ruchi Css -->
    <link rel="stylesheet" href="./assets/css/style.css">
    <!-- My Css -->
    <link rel="stylesheet" href="./assets/css/mystyle.css">
    <!-- Jquery js -->
    <script src="./assets/jquery/jquery.js"></script>
    <!-- Animate Css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@1,300;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,700;0,900;1,300;1,700&display=swap" rel="stylesheet">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <!-- =========Swiper===== -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" />
    <!-- ========Favicon====== -->
    <link rel="shortcut icon" href="./assets/images/newfav.png"  type="image/x-icon">
    
<script src="assets/js/bootstrap.bundle.js"></script>

</head>

<body>


    <header>
        <nav class="header_box px-5   ">
            <div class="logo">
                <!-- <h1 class="text-white"> -->
                    <img src="./assets/images/logoflat.png"  class="img-fluid" width="120px" height="90px" alt="">
                    <!-- IT-SOFT<b style="color: #3EDBF0;"> AGENCY</b> -->
                <!-- </h1> -->
            </div>
            <div class="nav_icon container-fluid">
                <li><a href="index.php">HOME</a></li>
                <li><a href="company.php">COMPANY</a></li>
                <li><a href="solution.php">SOLUTIONS</a></li>
                <li><a href="hire_developers.php">HIRE DEVELOPERS</a></li>
                <li><a href="technology.php">TECHNOLOGIES</a></li>
                <li><a href="portfolio.php">PORTFOLIO</a></li>
                <li><a href="contact.php">CONTACT</a></li>
            </div>
            <div class="mobile_number text-white d-none d-md-block">
                <a href="#"> <span style="color: #3EDBF0; ">1-800</span>-123-4567</a>

            </div>

            <i class="menu_btn fas fa-bars text-white"></i>


        </nav>

    </header>