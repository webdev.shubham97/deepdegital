// -------- CHANGE HAMBURGER BUTTON -------

$('.navbar .menu li a').click(function () {
    // applying again smooth scroll on menu items click
    $('html').css("scrollBehavior", "smooth");
  });

  // toggle menu/navbar script
  $('.menu-btn').click(function () {
    $('.nav').toggleClass("active");
    $('.menu-btn i').toggleClass("active");
  });

//   ------- DOUGHNUT --------
var xValues = ["Direct", "Affiliate", "Email", "Other"];
var yValues = [55, 49, 44, 24];
var bgColors = [
  "#b91d47",
  "#00aba9",
  "#2b5797",
  "#e8c3b9"
];

new Chart("mydoughnut", {
  type: "doughnut",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: bgColors,
      data: yValues,
      labels: xValues
    }]
  },
  options: {
    title: {
      display: false,
      text: "World Wide Wine Production 2018"
    }
  }
});

// ------- CHART -------

var xValues = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];

new Chart("myChart", {
  type: "line",
  fill: "#000",
  data: {
    labels: xValues,
    datasets: [{
      data: [860, 1140, 1060, 1060, 1070, 1110, 1330, 2210, 7830, 2478],
      borderColor: "red",
      fill: false
    }, {
      data: [1600, 1700, 1700, 1900, 2000, 2700, 4000, 5000, 6000, 7000],
      borderColor: "green",
      fill: false
    }, {
      data: [300, 700, 2000, 5000, 6000, 4000, 2000, 1000, 200, 100],
      borderColor: "blue",
      fill: false
    }]
  },
  options: {
    legend: { display: false }
  }
});

// ------- CHANGE HEADER --------

$(window).scroll(function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 150) {
      $("header").addClass("background-header");
    } else {
      $("header").removeClass("background-header");
    }
  });

//   ------- WORLD MAP -----

$(document).ready(function () {

    $(".country").hover(function () {

      var gmt_country = $(this).attr("id");
      var options = {
        format: '<span class=\"dt\">%A, %d %B %I:%M:%S %P</span>',
        timeNotation: '12h',
        am_pm: true,
        utc: true,
        utc_offset: gmt_country
      };
      // $('.jclock').jclock(options);

      // var country_time = $(".jclock").text();

      // $(this).attr("data-content", country_time);

    });

    $('[data-toggle="popover"]').popover({ trigger: "hover" });

  });

//   ----- GOOGLE CHART -----

google.charts.load('current', { 'packages': ['corechart'] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Month', 'Sales', 'Expenses'],
    ['feb', 1000, 400],
    ['march', 1170, 460],
    ['april', 660, 1120],
    ['may', 1030, 540]
  ]);

  var options = {
    title: '',
    hAxis: { title: '', titleTextStyle: { color: '#333' } },
    vAxis: { minValue: 0 }
  };

  var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}

// ------- COLUMN CHART ---------

window.onload = draw();
function draw() {
  /* Accepting and seperating comma seperated values */
  var values = [88, 49, 100, 130, 70];

  var canvas = document.getElementById('myCanvas');
  var ctx = canvas.getContext('2d');

  var width = 10; //bar width
  var X = 50; // first bar position 
  var base = 100;

  for (var i = 0; i < values.length; i++) {
    ctx.fillStyle = '#008080';
    var h = values[i];
    ctx.fillRect(X, canvas.height - h, width, h);

    X += width + 15;
    /* text to display Bar number */
    ctx.fillStyle = '#4da6ff';
    // ctx.fillText('Bar '+i,X-50,canvas.height - h -10);
  }
  /* Text to display scale */
  ctx.fillStyle = '#000000';
  ctx.fillText('Scale X : ' + canvas.width + ' Y : ' + canvas.height, 800, 10);

}

// ------ CHART --------

var xValues = [400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900];
var yValues = [1, 3, 5, 5, 7, 8, 8, 9, 7, 8, 10];

new Chart("companyChart", {
  type: "line",
  data: {
    labels: xValues,
    datasets: [{
      fill: false,
      lineTension: 0,
      backgroundColor: "rgba(0,0,255,1.0)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: { display: false },
    labels: { display: false },
    data: { display: false },
    scales: {
      yAxes: [{ ticks: { min: 1, max: 10 } }],
    }
  }
});