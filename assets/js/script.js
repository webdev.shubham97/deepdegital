// Ruchi Sharma


var total_amount = 0;
var pricing = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 
18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31] ;

// var pricing = [0,'$ 1','$ 2','$ 3','$ 4','$ 5','$ 6','$ 7','$ 8','$ 9','$ 10','$ 11','$ 12','$ 13','$ 14','$ 15','$ 16','$ 17',
// '$ 18','$ 19','$ 20','$ 21','$ 22','$ 23','$ 24','$ 25','$ 26','$ 27','$ 28','$ 29','$ 30','$ 31'];


var amount = 0;
var present_cost = 0;

$('.select-toggle').click(function(){
    if(this.value == '1000'){
        present_cost = 0;
    }
    else if(this.value == '0'){
        present_cost = 0;
    }
    else{
        present_cost = pricing[this.value];
    }
}).change(function(){
    var price_section = $(this).next().children('pre');
    var amount_section = $(this).next().children('i');
    // console.log(price_section);
    
    if(this.value!='0'){
        
        amount_section.removeClass('fa-times text-danger');
        amount_section.addClass('fa-check text-success');
        $(price_section).html("$ "+pricing[this.value]);
        amount = pricing[this.value]
        total_amount += amount;
        $('#cart-amount').html('<i class="fas fa-cart-plus text-dark"></i>'+'$ '+ total_amount);
        console.log('from select '+ total_amount);
    }
    else if(this.value =='0' && present_cost != 0){
        amount_section.removeClass('fa-check text-success');
        amount_section.addClass('fa-times text-danger');
        $(price_section).html('$ 00');
        total_amount = total_amount - present_cost;
        console.log(total_amount);
        $('#cart-amount').html('<i class="fas fa-cart-plus text-dark"></i>'+'$ '+ total_amount);
        // amount = 0;
    
    }
    else if(this.value =='0' && present_cost == 0){
        amount_section.removeClass('fa-check text-success');
        amount_section.addClass('fa-times text-danger');
        $(price_section).html('$ 00');
        total_amount = total_amount - amount;
        $('#cart-amount').html('<i class="fas fa-cart-plus text-dark"></i>'+'$ '+ total_amount);
        amount = 0;
    
    }

});



var amount2 = 0;
var this_input_price_is;
var pricing2 = [0,2,4,6];
var present_cost2 = 0;

$('.number-input').click(function(){
    if(this.value.length == 0){
        present_cost2 = 0;

    }
    else{
        // console.log('length '+this.value.length);
        present_cost2 = this.value * pricing2[this.id];
    }
    
}).keyup(function(){
    total_amount = total_amount - amount2;
    present_cost2 = amount2;
    amount2 = 0;
    
        var price_section = $(this).next().children('pre');
        var amount_section = $(this).next().children('i');
        // console.log(this.value);
        // console.log('price '+this_input_price_is+ 'amount2'+ amount2);
        
        if(this.value != ''){
            
            amount2 = this.value*pricing2[this.id];
            amount_section.removeClass('fa-times text-danger');
            amount_section.addClass('fa-check text-success');
            $(price_section).html("$ "+ pricing2[this.id]);
            total_amount += amount2;
            $('#cart-amount').html('<i class="fas fa-cart-plus text-dark"></i>'+'$ '+ total_amount);
           
        }
    
      
        // else if(this.value > 0 && present_cost2 != 0){
        //     total_amount = total_amount - present_cost2;
        //     present_cost2 = 0;
        //     amount_section.removeClass('fa-times text-danger');
        //     amount_section.addClass('fa-check text-success');
        //     $(price_section).html("$ "+ pricing2[this.id]);
        //     total_amount = total_amount + amount2;
        //     $('#cart-amount').html('<i class="fas fa-cart-plus text-dark"></i>'+'$ '+ total_amount);
           
        // }
      
        else if(this.value == ''){
            // console.log('amount is '+amount2);
            amount_section.removeClass('fa-check text-success');
            amount_section.addClass('fa-times text-danger');
            $(price_section).html('$ 00');
            total_amount = total_amount - amount2;
        $('#cart-amount').html('<i class="fas fa-cart-plus text-dark"></i>'+'$ '+ total_amount);
        amount2 = 0;
       
        }
    });

